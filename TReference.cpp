/*
  class/structure TReference.cpp zawierajacy informacje
   dotyczaca opracowania wynikow 
   w tym polozenia poczatkow pikow

  J.Szabelski 17/01/2016
*/

#include"TReference.h"

   ClassImp(TReference);

// **********************
  TReference::TReference(void)
 {
  przegladane = NULL;
  wybrane = NULL;

  ListFADC = NULL;
  TAp = NULL;
  Tfile = NULL;
  drzewo = NULL;
  Evnt = NULL;
  Sel = NULL;
//evntRys = NULL;
//rob = NULL;


  Reset();
 }

// **********************
  void TReference::Reset(void)
 {
  nrPrzypadku = -1;
  
  t70 = -1;
  kNrPrzypadku = -1;

  Float_t *r;
  r = Poziom0_50_100; for (Int_t i=0;i<N_FADC_K;i++) {*r++ = 0.0;}
  bPoziom0 = kFALSE;
  bJestTrigger = kFALSE;

  Bool_t *b; 
  b=bTrigger; for (Int_t i=0;i<N_FADC_K;i++) {*b++ = kFALSE;}
  b=bTriggerOK; for (Int_t i=0;i<N_FADC_K;i++) {*b++ = kFALSE;}

  Int_t *a;
//a = kDlugoscImpPik; for (Int_t  i=0;i< N_FADC_K; i++) { *a++ = 0;}
  a = fadcLiczbaImp; for (Int_t  i=0;i< N_FADC_K; i++) { *a++ = 0;}
  a = fadcLiczbaNarastanImp; for (Int_t  i=0;i< N_FADC_K; i++) { *a++ = 0;}
  UShort_t *us;
  us = poczImp[0]; for (Int_t  i=0;i< N_FADC_K * N_IMP_L; i++) {*us++ = (UShort_t)0;}

  UChar_t *uc = ucMaxADC;
  for (Int_t i=0;i<N_FADC_K;i++) {*us++ = (UChar_t)0;}
  a = ixMaxADC; for (Int_t i=0;i<N_FADC_K;i++) { *a++ = 0;}
  r = srMaxPik; for (Int_t i=0;i<N_FADC_K;i++) {*r++ = 0.0;}
  a = ixSrMaxPik; for (Int_t i=0;i<N_FADC_K;i++) { *a++ = 0;}


  bJestMaxPik = kFALSE;

  a = kDlugoscImpPik; for (Int_t i=0;i<N_FADC_K;i++) { *a++ = 0;}
  bSaDlugosciPik = kFALSE;  

 }

// ^^^^^^^^^^^^^^^^^^^^^^

