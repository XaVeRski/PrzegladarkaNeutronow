/*
  header TGGuzikiJS.h for
  class TGGuzikiJS.cpp to make
    TGButton's table for decission

  K.Jedrzejczak & J.Szabelski 4/2/2003
  J.Szabelski 16/5/2005
  J.Szabelski 11/1/2016, 23/3/2016
*/

#ifndef JS_GUZIKI_04
#define JS_GUZIKI_04

#include "TApplication.h"
#include "TSystem.h"

#include "TGOdpowiedz04.h"

/* 0000000000000000000000000000000000000000000 */
class TGGuzikiJS : public TObject
{
private:
    TApplication *theAppl;

    Int_t kNacisniety;    // numer nacisnietego (wybranego) pola: 1,2, ..,6,7,8

    Bool_t bNumerOK;
    Char_t cKtoryNastepny[8];
    Int_t kNumerNastepnegoPrzypadku;   // numer przypadku wpisanego w pole i wybrane pole 6

    Char_t cTriggerNo[2];
    Int_t kNumerKanaluTriggera;   // numer kanalu triggera (wybrane 8, pierwsze pole)
    Char_t cPulseNo[4];
    Int_t kLiczbaPulsow;   // liczba pulsow (wybrane 8, drugie pole)

    void Wodotrysk(void);

    Bool_t CzyToLiczba(Char_t *cc);
    Int_t ChIconv(Char_t *cc);

public:
    TGOdpowiedz04 *Bny;
    TGGuzikiJS(Int_t &qkNacisniety, TApplication *qtheAppl);
    ~TGGuzikiJS(void);
    Int_t GetNacisniety(void) {return kNacisniety;};
    Int_t GetNumer(void) {return kNumerNastepnegoPrzypadku;};
    Int_t GetTriggerCh(void) {return kNumerKanaluTriggera;};
    Int_t GetPulseNo(void) {return kLiczbaPulsow;};

    /* =========================================== */
public:
    ClassDef(TGGuzikiJS,0);
};

#endif


