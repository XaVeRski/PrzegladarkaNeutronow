/*
  header TGOdpowiedz04.h for
  class TGOdpowiedz04.cpp to make
    Canvas for decission buttons
   and returns decission

  based on TGOdpowiedz01 by
  K.Jedrzejczak & J.Szabelski 4/2/2003
  J.Szabelski 16/5/2005
  J.Szabelski 19/5/2005

  by:
  K. Wojczuk 22/10/2016, 21/3/2016
  J. Szabelski 11/1/2016, 23/3/2016
*/

#ifndef JS_ODP_BD_04
#define JS_ODP_BD_04

#include "string.h"

//#include "TString.h"

#include "TGClient.h"
#include "TGButton.h"

#include "TGLabel.h"
#include "TGTextEntry.h"
#include "TApplication.h"
#include "TGComboBox.h"
#include "TGButtonGroup.h"

/* 0000000000000000000000000000000000000000000 */
  class TGOdpowiedz04
    : public TGMainFrame
 {
  private:
    TApplication *theAppl;

    TGVerticalFrame *fVert;
    TGHorizontalFrame *fHorNumb;

    TGTextButton *fGuzikNextSelected;
    TGTextButton *fGuzikPreviousSelected;
    TGTextButton *fGuzikThisOne;
    TGTextButton *fGuzikNext;
    TGTextButton *fGuzikPrevious;
    TGTextButton *fGuzikGiveNumber;
    TGTextButton *fGuzikQuit;
    TGTextButton *fGuzikSelect; //mod

    TGTextBuffer *fTekst;
    TGTextEntry  *fWpisLiczbe;

    TGLayoutHints   *fLayout;
    TGLayoutHints   *fLayoutNumer;

    TGHorizontalFrame *fHorSelect;
    TGLayoutHints *fLayoutSelect;

    TGHorizontalFrame *fRadioChanelSelect;
    TGLayoutHints *fLayoutRadioChSelect;
    TGButtonGroup *fRadioGrCh;
    TGRadioButton *fRadioCh[8];

    TGHorizontalFrame *fRadioPulseNoSelect;
    TGLayoutHints *fLayoutRadioPulseNoSelect;
    TGButtonGroup *fRadioGrPulseNo;
    TGRadioButton *fRadioPulseNo[5];


    TGTextBuffer *fTekstChannel;
    TGTextEntry *fWpisChannel;
    TGTextBuffer *fTekstNPuls;
    TGTextEntry *fWpisNPuls;

    Int_t kNacisniety;
    Char_t cKtoryNastepny[8]; //mod
    Int_t kRadioButtomChannel;
    Int_t kRadioButtomPulseNo;

  public:

        TGOdpowiedz04(const TGWindow *p, UInt_t w, UInt_t h, TApplication *qtheAppl);
       ~TGOdpowiedz04(void);
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
    Int_t GetNacisniety(void) {return kNacisniety;};
    Char_t *GetNumberString(void) {return cKtoryNastepny;}
    Int_t GetTrigChannel(void) {return kRadioButtomChannel;};
    Int_t GetPulseNumber(void) {return kRadioButtomPulseNo;};


/* =========================================== */
   public:
   ClassDef(TGOdpowiedz04,0);
 };

#endif


