/*
  class/structure TSelected.cpp zawierajacy numery wybranych przypadkow

  J.Szabelski 30/11/2012
*/

#include"TSelected.h"

   ClassImp(TSelected);

// **********************
  TSelected::TSelected(Char_t *name)
 {
  nSel = 0;
  status = 1;
  p = fopen(name, "r");
  if (!p) {status = -100; return;}
  printf("TSelected::TSelected: p=%p\n", p);

  CzytajSelected(); 
  if(p) {fclose(p); p=NULL;}
 }

// **********************
  Int_t TSelected::CzytajSelected(void)
 {
  printf("TSelected::CzytajSelected: poczatek\n");
  Int_t i =0;

  //Zmieniony by byl dopasowany do formatu funkcji wyboru, KW
  Char_t buf[128];

  while (fgets(buf, 127, p)) {

    if (buf[0] != '#') {
      Int_t i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16;
      Int_t j1, j2;
      Int_t x1;
      Long64_t x2;
      Int_t k1 = sscanf(buf, " %d %lld %d %d %d %d %d %d %d %d",
                              &x1, &x2, &j1, &j2, &i1, &i2, &i3, &i4, &i5, &i6);
      if (k1 != 10) {
        printf(" TSelected::CzytajSelected: ERROR:  k1 = %d != 10\n", k1);
        return -k1;
       }

      Int_t k2 = sscanf(&buf[50], " %d %d %d %d %d %d %d %d %d %d",
                             &i7, &i8, &i9, &i10, &i11, &i12, &i13, &i14, &i15, &i16);
      if (k2 != 10) {
        printf(" TSelected::CzytajSelected: ERROR:  k2 = %d != 10\n", k2);
        return -k2;
       }

      Int_t k = k1 + k2; 
      if (k != 20) {
        printf("TSelected:CzytajSelected: i=%d  k=%d\n", i, k);
        status = -1;
        return -1;
       }
      nWybrane[i] = x1;
      torWybrany[i] = (Char_t)j1;

      i++;
     }
   }
  nSel = i;  // liczba wybranych (z pliku)
  printf("TSelected::CzytajSelected: koniec: nSel=%d\n", nSel);
  return i;
 }



// **********************
//     nastepny wybrany po k-tym, ixN - index w nWybranych
  Int_t TSelected::GetNastepny(const Int_t k, Int_t &ixN)
 {
  Int_t *a; a = nWybrane;
  Int_t j = -1;
  for (Int_t i=0;i<nSel;i++) {
    if (k < (*a)) {
      j = i; break;
     }
    a++;
   }
  if (j == -1) { // nie ma wiekszego
    ixN = -1; 
    return -1;
   }

  kSel = j;
  ixN = j;
//printf("TSelected::GetNastepny: j=%d ixN=%d *a=%d nWybrane[%d]=%d\n", j, ixN, *a, j, nWybrane[j]); 
  return *a;
//return nWybrane[j];
 }


// **********************
//     poprzedzajacy wybrany po k-tym, ixN - index w nWybranych
  Int_t TSelected::GetPoprzedni(const Int_t k, Int_t &ixN)
 {
  Int_t *a; a = nWybrane + nSel - 1;
  Int_t j = -1;
  for (Int_t i=nSel-1;i>=0;i--) {
    if (k > (*a)) {
      j = i; break;
     }
    a--;
   }
  if (j == -1) { // nie ma mniejszego
    ixN = -1; 
    return -1;
   }

  kSel = j;
  ixN = j;
//printf("TSelected::GetNastepny: j=%d ixN=%d *a=%d nWybrane[%d]=%d\n", j, ixN, *a, j, nWybrane[j]); 
//return nWybrane[j];
  return *a;
 }

// **********************
//     sprawdza, czy przypadek k jest na liscie nWybrane (SELECTED)
  Bool_t TSelected::CzyWybrany(const Int_t k, Int_t &ixN)
 {
  Int_t i = 0;
  Int_t j = -1;
  Bool_t bWybrany = kFALSE;
  while (i<nSel) {
    if (nWybrane[i] == k) {  // to jest ten i jest "wybrany"
      bWybrany = kTRUE;
      j = i;
      break;
     }
    if (nWybrane[i] > k) {break;}  // juz sa wyzsze numery
    i++;
   }
  ixN = j;
  return bWybrany;
 }

// ^^^^^^^^^^^^^^^^^^^^^

