/*
  class TRysFADC.cpp to plot
    primitive histograms from FADC

  J.Szabelski 20/06/2005
  2/2/2013
*/

#include"TRysFADC.h"

   ClassImp(TRysFADC);

/*******************************
  constructor
-----------------------------*/
//TRysFADC::TRysFADC(TCzytPikiTree *qpikiROOT)
  TRysFADC::TRysFADC(TReference *qref)
 {
  ref = qref;

//pikiROOT = qpikiROOT;
//evPiki = pikiROOT->evPiki;
  retPiki = -3;

  for (Int_t i=0;i<8;i++) { F[i] = NULL;}
  lFADC = 32768;
//trFADC = 11718;
  trFADC = 1536;
  nanosec_per_pixel = 100;
//trFADC = 11776;
//nanosec_per_pixel = 800;

  nrPrzypadku = 0;


  Zeros();
 }
/* ^^^^^^^^^^^^^^^^^ */
/*******************************
  destructor
-----------------------------*/
  TRysFADC::~TRysFADC(void)
 {
  if (ttADC) {delete ttADC; ttADC=NULL;}
  if (ttPoz) {delete ttPoz; ttPoz=NULL;}
  if (mMaxADC) {delete mMaxADC; mMaxADC=NULL;}
  for (Int_t i=0;i<8;i++) {
    if (hhFadc[i]) {delete hhFadc[i]; hhFadc[i] = NULL;}
//  if (cFadc[i]) {delete cFadc[i]; cFadc[i] = NULL;}
    if (lPix0[i]) {delete lPix0[i]; lPix0[i] = NULL;}
    for (Int_t k=0; k<N_IMP_L; k++) {
      if (lPixPick[i][k]) {delete lPixPick[i][k], lPixPick[i][k]=NULL;}
     }
   }
  if (c8) {delete c8; c8=NULL;}
  if (cSel) {delete cSel; cSel=NULL;}
  if (hSel) {delete hSel; hSel=NULL;}
 }
/* ^^^^^^^^^^^^^^^^^ */

/*******************************
    Zeros drawing classes
-----------------------------*/
   void TRysFADC::Zeros(void)
 {
  for (Int_t i=0;i<8;i++) {
    hhFadc[i] = NULL;
//  cFadc[i] = NULL;
    lPix0[i] = NULL;
    padFADC[i]=NULL;
    for (Int_t k=0; k<N_IMP_L; k++) { lPixPick[i][k]=NULL;} 
   }
  c8 = NULL;
  ttADC = NULL;
  ttPoz = NULL;
  mMaxADC = NULL; 
  torWybrany = (Char_t)-1;
  adcMax = -1.;
  adcMaxPoz = -1;
  cSel = NULL;
  hSel = NULL;
 }
/* ^^^^^^^^^^^^^^^^^ */


/*******************************
    make 8 histograms
 returns:
  0  OK
 -1  ih outside limit
-----------------------------*/
   Int_t TRysFADC::Draw8H(TList *listaFADC)
 {
  Int_t nFADC = listaFADC->GetEntries();
  for (Int_t i = 0; i< nFADC; i++) {
    F[i] = (TFadcData *)listaFADC->At(i);
   }

  
  for (Int_t i = 0; i< 8; i++) {
    if (padFADC[i]) {delete padFADC[i]; padFADC[i]=NULL;}
   }
  if (c8) {delete c8; c8 = NULL;}

//retPiki = pikiROOT->readEvent(nrPrzypadku);
  nrPrzypadku = ref->nrPrzypadku;
  

  Char_t nazwCanv [128];
  sprintf(nazwCanv, "przypadek %5d      %s", nrPrzypadku, ref->Evnt->ch_czas);
//sprintf(nazwCanv, "przypadek %5d      %s", nrPrzypadku, data_czas);
//printf("TRysFADC::Draw8H: nazwCanv **%s**\n", nazwCanv); 

  c8 = new TCanvas(nazwCanv, nazwCanv , 20, 20, 800, 800);
  c8->Divide(2,4);

  for (Int_t i=0; i< nFADC; i++) {
    Int_t j=i/2;
    Int_t ii = i - 2*j;
    Char_t nazwPad[16];
    sprintf(nazwPad,"przebieg%1d", i);
    padFADC[i] = new TPad(nazwPad, nazwPad, ii*0.5, 1.0 - (j+1)*0.25, (ii+1)*0.5 - 0.01, 1.0 - j*0.25-0.01); 
    padFADC[i]->Draw();
   }

  for (Int_t i=0; i< nFADC; i++) {
    padFADC[i]->cd();
    if (hhFadc[i]) {delete hhFadc[i]; hhFadc[i] = NULL;}
    Char_t nazwH[14];
    Char_t nazwP[16];
    sprintf(nazwP,"przebieg %1d", i);
    sprintf(nazwH,"Hprzebieg%1d", i);
    hhFadc[i] = new TH1S(nazwH, nazwP, lFADC, 0, lFADC);
    hhFadc[i]->SetStats(0);
    UChar_t *a; a = F[i]->FADC;
    for (Int_t n=0;n< lFADC; n++) {
      hhFadc[i]->SetBinContent(n+1,(Double_t)(*(a+n)) );
     }

        // opisy osi
    OpisyOsi(i, hhFadc[i]);

    hhFadc[i]->Draw();
    padFADC[i]->Update();

     // linia triggera
    LiniaTriggera(i, padFADC[i]);

    LiniePikow(i, padFADC[i]);

    InfoPisana(i, padFADC[i]);
   }
  return 0;
 }

// *******************************
   Int_t TRysFADC::DrawSelected(const Int_t i1, const Int_t i2)
 {
  if (cSel) {delete cSel; cSel=NULL;}
  if (hSel) {delete hSel; hSel=NULL;}
  Int_t kTor = (Int_t)torWybrany;
  if (kTor<0) {return 0;}

  Char_t cNazwSel[32];
  sprintf(cNazwSel, "przebieg %1d", kTor);
  hSel = new TH1S("hSel", cNazwSel, i2-i1+1, i1, i2);
  hSel->SetStats(0);

  for (Int_t i=i1; i<i2; i++) {
    hSel->Fill((Double_t)i, hhFadc[kTor]->GetBinContent(i));
   }

  cSel = new TCanvas("cSel", cNazwSel, 850, 40, 500, 350);

  hSel->Draw();
  TPad *ppad = (TPad*)cSel->cd();

  OpisyOsi(-1, hSel);
//TVirtualPad* ppad = gPad;
  LiniaTriggera(kTor, ppad);
  InfoPisana(kTor, ppad);

  cSel->Update();
  return 1;
 }


// *******************************
    void TRysFADC::OpisyOsi(const Int_t i, TH1S *h)
 {
  if ( ((Int_t)(i/2) *2 == i) || (i== -1)) {
    h->SetYTitle("ADC");
    h->SetTitleSize(0.045, "Y");
   }
  if ( (i==6 || i==7) || (i== -1) ) {
    h->SetXTitle("time (100ns units)");
    h->SetTitleSize(0.045, "X");
   }
  h->SetLabelSize(0.045, "X");
  h->SetLabelSize(0.045, "Y");
 }



/*******************************
    rysuje linie triggera
-----------------------------*/
    void TRysFADC::LiniaTriggera(const Int_t i, const TPad *pad)
 {
  Double_t hmin = pad->GetUymin();
  Double_t hmax = pad->GetUymax();

//printf("TRysFADC::LiniaTrigerra: i=%d  hmin=%g  hmax=%g\n", i, hmin, hmax);

  if (!lPix0[i]) {
    lPix0[i] = new TLine((Double_t)trFADC,hmin,(Double_t)trFADC,hmax);
   }
  lPix0[i]->SetLineColor(kRed);
  lPix0[i]->SetLineWidth(2);
  lPix0[i]->Draw("SAME");

//hhFadc[i]->GetListOfFunctions()->Add(lPix0[i]);
 }


/*******************************
    rysuje linie poczatkow pikow
-----------------------------*/
    void TRysFADC::LiniePikow(const Int_t i, const TPad *pad)
 {
  if (retPiki <= 0) {return;}
  Double_t hmin = pad->GetUymin();
  Double_t hmax = pad->GetUymax();

//printf("TRysFADC::LiniaTrigerra: i=%d  hmin=%g  hmax=%g\n", i, hmin, hmax);
  Int_t fadcLiczbaNarastanImp = ref->fadcLiczbaNarastanImp[i];
  if (fadcLiczbaNarastanImp > 0) {
    for (Int_t k=0; k<fadcLiczbaNarastanImp; k++) {
      UShort_t pik = ref->poczImp[i][k];
      Double_t hhmax = TMath::Min(20.0, hmax);
      if (!lPixPick[i][k]) {
        lPixPick[i][k] = new TLine((Double_t)pik,hmin,(Double_t)pik,hhmax);
       }
      lPixPick[i][k]->SetLineColor(kGreen);
      lPixPick[i][k]->SetLineWidth(2);
      lPixPick[i][k]->Draw("SAME");
     }
   }
//hhFadc[i]->GetListOfFunctions()->Add(lPix0[i]);
 }

/*******************************
    wpisuje na pad informacje o impulsie
-----------------------------*/
  void TRysFADC::InfoPisana(const Int_t i, const TPad * )
 {
  if (i != (Int_t)torWybrany) {return;}  // tylko dla wybranego toru

  Char_t cADC[16], cPolMaxADC[16];
  sprintf(cADC, " %5.1f", adcMax);
  sprintf(cPolMaxADC, " %5d", adcMaxPoz);
  cADC[6] = (Char_t)0;
  cPolMaxADC[6] = (Char_t)0;

  if (!ttADC) { ttADC = new TText(0.7, 0.8, cADC);}
  if (!ttPoz) { ttPoz = new TText(0.7, 0.7, cPolMaxADC);}

  ttADC->SetNDC();
  ttPoz->SetNDC();

  ttADC->Draw();
  ttPoz->Draw();

  if (!mMaxADC) { mMaxADC = new TMarker((Double_t)adcMaxPoz, (Double_t)adcMax, 21);}
  mMaxADC->SetMarkerColor(kMagenta);
  mMaxADC->Draw();
 }

//lFADC = 32768;
//trFADC = 1536;
//nanosec_per_pixel = 100;

