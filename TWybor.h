/*
  header TWybor.h for
  class TWybor.cpp with selection of pressed buttom
  and action

  J.Szabelski 23/1/2016
*/

#ifndef JS_EV_WYBOR_01
#define JS_EV_WYBOR_01

#include "TReference.h"

#include "TGGuzikiJS.h"
#include "TSelected.h"
//#include "TEvent.h"
#include "TPrzemial.h"

/* 0000000000000000000000000000000000000000000 */
  class TWybor
 {
  private:
  TReference *ref;

  Int_t NoOfN;
  TGGuzikiJS *G;
  TSelected *Sel;
  TApplication *TAp;

  TEvent *Evnt;
  TPrzemial *rob;

  FILE *wybrane;

  Int_t iWybrany; // numer nastepnego przypadku wygnany guzikiem "select"
  Int_t iTriggChannel; // numer kanalu z triggerowym pulsem
  Int_t iPulseNo; // liczba pulsow w przypadku

  public:

  Int_t iOstatni;  // nr poprzedniego wyswietlanego przebiegu
  Int_t iSelected;  // nr wybranego przypadku w klasie TSelected
  Int_t kSelected;   // licznik wybranych
  Int_t kNowy;



     TWybor(TReference *qref);
//   TWybor(TSelected *qSel, TApplication *qTAp, FILE *qwybrane, const Int_t qNoOfN);
    ~TWybor(void);

//Int_t WyborGuzikiem(const Int_t iAktualny, TEvent *qEvnt);
  Int_t WyborGuzikiem(const Int_t iAktualny);
  Int_t Event_Analiza_Zapis(const Int_t iAktualny);

/* =========================================== */
   public:
   ClassDef(TWybor,0);
 };

#endif


