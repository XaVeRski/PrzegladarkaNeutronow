/*
  header TJSsubT70.h
  for TJSsubT70.cpp (based on TJSsub.cpp)
 procedures (several C++/ROOT classes) for many purposes:

   class Tch2tm // ASCII to tm structure
   class Tch2t70UTC     // ASCII time (UTC) to UTC t70
   class Tch2t70Loc      // ASCII time (local) to UTC t70
   class Tch2t70        // ASCII time (local or UTC)  to UTC t70

   class Tt70_2dattime  // t70  to (UCT or local) ASCII time
   class Tdate2t70     // integers: YYYY, MM, DD, hh, mm, ss (UTC or local)
                 // to t70 (UTC)
   class Tt70_2date   // t70 to (UTC or local) time in integer form:
                   //  YYYY, MM, DD, hh, mm, ss

   class Tascii_date // converts ASCII time to integer time and opposite:
                 // yyyymmddhhmmss <--> (integers: YYYY, MM, DD, hh, mm, ss)

     Jacek Szabelski
     28/9/2003, modyf. 1/11/2007, 10/5/2011
*/

#ifndef _JS_SUBR_T70
#define _JS_SUBR_T70

//#include <stdio.h>
#include <string.h>
#include <time.h>

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ time t70 manipulations 

 class Tch2tm         // ascii time (GMT) to tm structure
    : public TObject
 {
//private:
   Int_t Ch2Int(const Char_t *ch, const Int_t n);
  public:
   Tch2tm(void){};
   ~Tch2tm(void){};
   void Ch2tm(const Char_t *yyyymmddhhmmss, tm &jstm);
/* ================= */
   public:
   ClassDef(Tch2tm,0);
 };

// ========================================
 class Tch2t70UTC         // ascii time (GMT) to GMT t70
    : public TObject
 {
  private:
// struct tm jstm;
   tm jstm;
   Tch2tm *C;
  public:
   Tch2t70UTC(void);
   ~Tch2t70UTC(void);
   Long_t UTCDateTime2t70(const Char_t *yyyymmddhhmmss);
/* ================= */
   public:
   ClassDef(Tch2t70UTC,0);
 };

// ========================================
 class Tch2t70Loc         // ascii time (Local) to GMT  t70
    : public TObject
 {
  private:
// struct tm jstm;
   tm jstm;
   Tch2tm *C;
  public:
   Tch2t70Loc(void);
   ~Tch2t70Loc(void);
   Long_t LocDateTime2t70(const Char_t *yyyymmddhhmmss);
/* ================= */
   public:
   ClassDef(Tch2t70Loc,0);
 };

// ========================================
 class Tch2t70         // ascii time (Local o UTC) to UTC  t70
    : public TObject
 {
  private:
// struct tm jstm;
   tm jstm;
   Tch2tm *C;
  public:
   Tch2t70(void);
   ~Tch2t70(void);
   Long_t LocDateTime2t70(const Char_t *yyyymmddhhmmss);
   Long_t UTCDateTime2t70(const Char_t *yyyymmddhhmmss);
/* ================= */
   public:
   ClassDef(Tch2t70,0);
 };

// ---------------------------------------
  class Tt70_2dattime      // t70  to (UCT or local) acsii time
    : public TObject
 {
  private:
// struct tm jstm;
   tm jstm;
  public:
   Tt70_2dattime(void){};
   ~Tt70_2dattime(void){};
   void t70_2UTCDateTime(Long_t t70,Char_t *yyyymmddhhmmss);  // ti UTC tme
   void t70_2LocDateTime(Long_t t70,Char_t *yyyymmddhhmmss);  // to local time
// =================   
   public:
   ClassDef(Tt70_2dattime,0);
 };

// ---------------------------------------
  class Tdate2t70      // int: Yyyy,Month,Day,hh,mm,ss (UTC or local) to  t70 (GMT)
    : public TObject
 {
  private:
    tm jstm;
    void ii2tm(const Int_t Yyyy, const Int_t Month, const Int_t Day,
        const Int_t hh, const Int_t mm, const Int_t ss);
//      struct tm *jstm);
  public:
   Tdate2t70(void){};
   ~Tdate2t70(void){};
   Long_t UTCDate2t70(const Int_t Yyyy, const Int_t Month,
     const Int_t Day, const Int_t hh = 0, const Int_t mm = 0, const Int_t ss = 0);
   Long_t LocDate2t70(const Int_t Yyyy, const Int_t Month,     // local time date-time
     const Int_t Day, const Int_t hh = 0, const Int_t mm = 0, const Int_t ss = 0);

// =================   
   public:
   ClassDef(Tdate2t70,0);
 };

// ---------------------------------------
  class Tt70_2date      // t70 --> int: Yyyy,Month,Day,hh,mm,ss (GMT)
    : public TObject
 {
  private:
   tm jstm;
   void jstm2int(
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss);
  public:
        Tt70_2date(void){};
       ~Tt70_2date(void){};
   void t70_2dateUTC(const Long_t t70,
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss);
   void t70_2dateLoc(const Long_t t70,            // for t70 to local time
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss);
// to be called:
//    Int_t Yyyy,Month,Day,hh,mm,ss;
//   .... ->t70_2dateUTC(t70,&Yyyy,&Month,&Day,&hh,&mm,&ss);

// =================    
   public:
   ClassDef(Tt70_2date,0);
 };

// ---------------------------------------
  class Tascii_date  // yyyymmddhhmmss  <--> int: Yyyy,Month,Day,hh,mm,ss
    : public TObject
 {
  private:
    Int_t Ch2Int(const Char_t *ch, const Int_t n);
  public:
        Tascii_date(void){};
       ~Tascii_date(void){};
   void ascii2date(const Char_t *yyyymmddhhmmss,
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss);
   void date2ascii(const Int_t Yyyy, const Int_t Month,
       const Int_t Day, const Int_t hh, const Int_t mm, const Int_t ss,
       Char_t *yyyymmddhhmmss);
// to be called:
//    Char_t yyyymmddhhmmss[16];   
//    Int_t Yyyy,Month,Day,hh,mm,ss;
//   .... ->ascii2date(yyyymmddhhmmss,&Yyyy,&Month,&Day,&hh,&mm,&ss);
//   .... ->date2ascii(Yyyy,Month,Day,hh,mm,ss,yyyymmddhhmmss);

// =================    
   public:
   ClassDef(Tascii_date,0);
 };


// ************************************
#endif


