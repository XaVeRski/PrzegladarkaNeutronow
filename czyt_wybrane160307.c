//  czyt_wybrane160307.c
// do czytania plikow "wybrane ...."
// i dalszej analizy

// J.Szabelski 7/3/2016

  const Int_t nData = 1000;  // max liczba analizowanych przypadkow
  Int_t kData;       // liczba przypadkow wczytanych z danych

  Int_t nrSerii[nData];
  Int_t nrPrzypadku[nData];
  Long64_t t70_Data[nData];
  Int_t triggCh[nData];
  Int_t nPulsow[nData];
  Int_t bgr[8][nData];
  Int_t adcMax[8][nData];

  const Int_t nPlikowInp = 1;
  Char_t fileName[nPlikowInp][128] = {
    "/big_disk/home/js/js/obl/LNGS/przegladarkaKW03/wybrane_gstpbpe001_20160203211411.txt"
   };
  Int_t kPlikowInp;

  TCanvas *cDeltaT;
  TH1F *hDeltaT;
  TCanvas *cKanalyTrig;
  TH1F *hKanalyTrig;


  void czyt_wybrane160307(void);
  Int_t wczytajWybrane(void);
  Int_t decode(Char_t *buf);
  void Wypisz(FILE *fPr = stdout);
  void Analiza(void);
  void zrobHist(void);
  void RysHists(void);
  void Rys_DeltaT(void);
  void Rys_KanalyTrig(void);


// ******************************
  void czyt_wybrane160307(void)
 {
  kPlikowInp = 0;
  kData = 0;

  while (kPlikowInp < nPlikowInp) {
    if (wczytajWybrane()) {
      printf(" czyt_wybrane160307: Input ERROR: kPlikowInp = %d\n", kPlikowInp);
      return;
     }
    kPlikowInp++;
   }

  Wypisz();

  Analiza();

 }


// ******************************
  Int_t wczytajWybrane(void)
 {
  Char_t *nazwa;
  nazwa = &fileName[kPlikowInp][0];
  FILE *fIN = fopen(nazwa, "r");
  Char_t buf[128];
  Int_t wiersz = 0;
 
  while (fgets(buf, 128, fIN)) {
    if (buf[0] != '#') {
      Int_t k = decode(buf);
      if ( k != 20) {
        printf(" wczytajWybrane: ERROR: plik# %d  wiersz= %d   k = %d != 20  kData = %d\n", kPlikowInp, wiersz, k, kData);
        return -2;
       }
      kData++;
      if (kData >= nData) {
        printf(" wczytajWybrane: ERROR: kData = %d >= %d = nData\n", kData, nData);
        printf("             zwieksz nData i czytaj jeszcze raz !!!! \n");
        return -1;
       }
     }
    wiersz++;
   }

  return 0;
 }

// ******************************
  Int_t decode(Char_t *buf)
 {
  Int_t a1;
  Long64_t l2;
  Int_t a3, a4, a5, a6, a7, a8, a9, a10,
        a11, a12, a13, a14, a15, a16, a17, a18, a19, a20;

   // sscanf ma ograniczenie do 12 elementow
  Int_t k1 = sscanf( buf,
   " %d %lld %d %d %d %d %d %d %d %d",
//    1    2  3  4  5  6  7  8  9 10 
   &a1, &l2, &a3, &a4, &a5, &a6, &a7, &a8, &a9, &a10);
  if (k1 != 10) {
    printf(" decode: ERROR:  k1 = %d != 10\n", k1);
    return k1;
   }

  Int_t k2 = sscanf( &buf[50],
   " %d %d %d %d %d %d %d %d %d %d",
//   11 12 13 14 15 16 17 18 19 20
   &a11, &a12, &a13, &a14, &a15, &a16, &a17, &a18, &a19, &a20);
  if (k2 != 10) {
    printf(" decode: ERROR:  k2 = %d != 10\n", k2);
    return k2;
   }

  Int_t k = k1 + k2;
  if (k != 20) { return k; }  

  nrSerii[kData] = kPlikowInp;
  nrPrzypadku[kData] = a1;
  t70_Data[kData] = l2;
  triggCh[kData] = a3;
  nPulsow[kData] = a4;
  bgr[0][kData] = a5;
  adcMax[0][kData] = a6;
  bgr[1][kData] = a7;
  adcMax[1][kData] = a8;
  bgr[2][kData] = a9;
  adcMax[2][kData] = a10;
  bgr[3][kData] = a11;
  adcMax[3][kData] = a12;
  bgr[4][kData] = a13;
  adcMax[4][kData] = a14;
  bgr[5][kData] = a15;
  adcMax[5][kData] = a16;
  bgr[6][kData] = a17;
  adcMax[6][kData] = a18;
  bgr[7][kData] = a19;
  adcMax[7][kData] = a20;
  
  return k;
 }

// ******************************
  void Wypisz(FILE *fPr)
 {
  fprintf(fPr, " liczba_plikow_wejsciowych = %d   liczba_przypadkow = %d\n", kPlikowInp, kData);
  for (Int_t i=0; i<kData; i++) {
    fprintf(fPr, " %2d  %4d %10lld %1d %2d",
        nrSerii[i], nrPrzypadku[i], t70_Data[i], triggCh[i], nPulsow[i]);
    for (Int_t j=0; j<8; j++) {
      fprintf(fPr, "  %3d %3d", bgr[j][i], adcMax[j][i]);
     }
    fprintf(fPr, "\n");
   }
 }

// ******************************
  void Analiza(void)
 {
  zrobHist();

  Long64_t t0 = 0;
  Long64_t dt;
  for (Int_t i=0; i<kData; i++) {
    dt = t70_Data[i] - t0;
    if (i) {
      hDeltaT->Fill((Double_t)dt);
     }
    t0 = t70_Data[i];

    hKanalyTrig->Fill(triggCh[i]);
   }
/*
  Int_t nrSerii[nData];
  Int_t nrPrzypadku[nData];
  Long64_t t70_Data[nData];
  Int_t triggCh[nData];
  Int_t nPulsow[nData];
  Int_t bgr[8][nData];
  Int_t adcMax[8][nData];
*/

  RysHists();
 }


// ******************************
  void zrobHist(void)
 {
  hDeltaT = new TH1F("hDeltaT", "delta t70", 500, 0, 500);
  hDeltaT->SetXTitle("#Delta t70 (sek)");
  hDeltaT->SetYTitle("n");

  hKanalyTrig = new TH1F("hKanalyTrig", "liczba triggerow", 10, -1, 9);
  hKanalyTrig->SetXTitle("nr kanalu");
  hKanalyTrig->SetXTitle("n triggerow");
 }

// ******************************
  void RysHists(void)
 {
  Rys_DeltaT();
  Rys_KanalyTrig();
 }


// ******************************
  void Rys_DeltaT(void)
 {
  cDeltaT = new TCanvas("cDeltaT", "cDeltaT", 600, 600);
  cDeltaT->SetLogy();
  hDeltaT->Draw();
  cDeltaT->Update();
 }


// ******************************
  void Rys_KanalyTrig(void)
 {
  cKanalyTrig = new TCanvas("cKanalyTrig", "cKanalyTrig", 600, 600);
  hKanalyTrig->Draw();
  cKanalyTrig->Update();
 }





