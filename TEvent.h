/*
  header TEvent.h for
  class/structure TEvent.cpp to keep one EAS event data

  J.Szabelski 14/1/2016
*/

#ifndef JS_1EVNT_DATA_02
#define JS_1EVNT_DATA_02

#include "TFadcData.h"

#define N_KANAL 8

/* 0000000000000000000000000000000000000000000 */
  class TEvent
    : public TObject
 {
  public:

  TFadcData *przeb2[N_KANAL];
  Char_t ch_czas[16];
//ULong64_t t70;
  UInt_t t70;

        TEvent(void);
       ~TEvent(void);

   void CopyTo(TEvent *G);
   void Reset(void);

/* =========================================== */
   public:
   ClassDef(TEvent,0);
 };

#endif


