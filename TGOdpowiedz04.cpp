/*
  class TGOdpowiedz04.cpp to make
    Canvas for decission buttons 
   and returns kNacisniety - nr nacisnietego guzika 
        liczac od 1 od gory

 based on TGOdpowiedz01 by
  K.Jedrzejczak & J.Szabelski 4/2/2003
  J.Szabelski 16/5/2005 

  by:
  K. Wojczuk 22/10/2016, 21/3/2016
  J. Szabelski 11/1/2016, 23/3/2016
*/

#include "TGOdpowiedz04.h"

   ClassImp(TGOdpowiedz04);

/*******************************
  constructor
-----------------------------*/
  TGOdpowiedz04::TGOdpowiedz04(
     const TGWindow *p, UInt_t w, UInt_t h, TApplication *qtheAppl)
    :TGMainFrame(p, w, h)
 {
  theAppl = qtheAppl;

  Char_t *c; c=cKtoryNastepny; for (Int_t i=0;i<8;i++){*c++=(Char_t)0;}
  kRadioButtomChannel = -2;
  kRadioButtomPulseNo = -2;

  fVert = new TGVerticalFrame(this, 60, 70);
//fVert = new TGVerticalFrame(this,160,20);

  fLayout = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY);  // kLHintsCenterX and kLHintsCenterY
                             // from TGLayout.h:
                             //  kLHintsCenterX = 2  = 0x02
                             //  kLHintsCenterY = 16 = 0x10
                             //  kLHintsCenterX | kLHintsCenterY = 18 = 0x12
                             // JS: nie wiem, o co chodzi ?

  Int_t i;
  i = 1;
  fGuzikNextSelected = new TGTextButton(fVert, "next selected", i);
  fGuzikNextSelected->Associate(this);
  fVert->AddFrame(fGuzikNextSelected, fLayout);
  i = 2;
  fGuzikPreviousSelected = new TGTextButton(fVert, "previous selected", i);
  fGuzikPreviousSelected->Associate(this);
  fVert->AddFrame(fGuzikPreviousSelected, fLayout);
  i = 3;
  fGuzikThisOne = new TGTextButton(fVert, "once again", i);
  fGuzikThisOne->Associate(this);
  fVert->AddFrame(fGuzikThisOne, fLayout);
  i = 4;
  fGuzikNext = new TGTextButton(fVert, "next event", i);
  fGuzikNext->Associate(this);
  fVert->AddFrame(fGuzikNext, fLayout);
  i = 5;
  fGuzikPrevious = new TGTextButton(fVert, "previous event", i);
  fGuzikPrevious->Associate(this);
  fVert->AddFrame(fGuzikPrevious, fLayout);

  i = 6;
  fHorNumb = new TGHorizontalFrame(fVert,50,20,4|8);
//fHorNumb = new TGHorizontalFrame(fVert,80,10,4|8);   // bez roznicy
  fLayoutNumer = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY,
      2,2,2,2 );

  fGuzikGiveNumber = new TGTextButton(fHorNumb, "give event number:", i);
  fGuzikGiveNumber->Associate(this);
  fHorNumb->AddFrame(fGuzikGiveNumber, fLayoutNumer);
  
	
  fTekst = new TGTextBuffer(64);
  fWpisLiczbe = new TGTextEntry(fHorNumb,fTekst);
  fWpisLiczbe->Resize(50, fWpisLiczbe->GetDefaultHeight());
  fHorNumb->AddFrame(fWpisLiczbe,fLayoutNumer);

  fVert->AddFrame(fHorNumb,fLayout);


  i = 7;
  fGuzikQuit = new TGTextButton(fVert, "quit", i);
  fGuzikQuit->Associate(this);
  fVert->AddFrame(fGuzikQuit, fLayout);

  i = 8; //mod

  fHorSelect = new TGHorizontalFrame(fVert,50,20,4| 8);
  fLayoutSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY,
      2,2,2,2 );

  //Select trigger channel then total number of pulses 
  fGuzikSelect = new TGTextButton(fHorSelect, "select event: trigger channel & NoOfPulses:", i);
  fGuzikSelect->Associate(this);
  fHorSelect->AddFrame(fGuzikSelect, fLayoutSelect);

  fVert->AddFrame(fHorSelect, fLayout);
  
// JS (24/3/2016):
// ------------------------------- trigger channel:
  fRadioChanelSelect = new TGHorizontalFrame(fVert, 50, 20, 4| 8);
  fLayoutRadioChSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY,
      2,2,2,2 );

  fRadioGrCh = new TGButtonGroup(fRadioChanelSelect, 0, 8, 0, 0, "trigger channel");
  for (Int_t k=0; k<8; k++) {
    Char_t chR[2];
    sprintf(chR, "%1d", k);
    fRadioCh[k] = new TGRadioButton(fRadioGrCh, chR, 100+k);
   }
  fRadioChanelSelect->AddFrame(fRadioGrCh, fLayoutRadioChSelect);

  fVert->AddFrame(fRadioChanelSelect, fLayout);

// ------------------------------- nunber of pulses:
  fRadioPulseNoSelect = new TGHorizontalFrame(fVert, 50, 20, 4| 8);
  fLayoutRadioPulseNoSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY,
      2,2,2,2 );

  fRadioGrPulseNo = new TGButtonGroup(fRadioPulseNoSelect, 0, 5, 0, 0, "no of pulses");
  for (Int_t k=0; k<5; k++) {
    Char_t chR[2];
    sprintf(chR, "%1d", k);
    fRadioPulseNo[k] = new TGRadioButton(fRadioGrPulseNo, chR, 200+k);
   }
  fRadioGrPulseNo->SetButton(201);
  fRadioPulseNoSelect->AddFrame(fRadioGrPulseNo, fLayoutRadioPulseNoSelect);

  fVert->AddFrame(fRadioPulseNoSelect, fLayout);
// -------------------------------


  AddFrame(fVert,fLayout);

  MapSubwindows();
  Layout();
  SetWindowName("Wodotrysk");
  SetIconName("wybory");
  MapWindow();

  Move(1200,40);

 }
/* ^^^^^^^^^^^^^^^^^ */
/*******************************
  destructor
-----------------------------*/
   TGOdpowiedz04::~TGOdpowiedz04(void)
 {

  for (Int_t i = 0; i<5; i++) {
    if (fRadioPulseNo[i]) { delete fRadioPulseNo[i]; fRadioPulseNo[i] = NULL;}
   }
  if (fRadioGrPulseNo) { delete fRadioGrPulseNo; fRadioGrPulseNo = NULL;}
  if (fLayoutRadioPulseNoSelect) { delete fLayoutRadioPulseNoSelect; fLayoutRadioPulseNoSelect = NULL;}
  if (fRadioPulseNoSelect) { delete fRadioPulseNoSelect; fRadioPulseNoSelect = NULL;}

  for (Int_t i = 0; i<8; i++) {
    if (fRadioCh[i]) { delete fRadioCh[i]; fRadioCh[i] = NULL;}
   }
  if (fRadioGrCh) { delete fRadioGrCh; fRadioGrCh = NULL;}
  if (fLayoutRadioChSelect) { delete fLayoutRadioChSelect; fLayoutRadioChSelect = NULL;}
  if (fRadioChanelSelect) { delete fRadioChanelSelect; fRadioChanelSelect = NULL;}

  if (fGuzikSelect) {delete fGuzikSelect; fGuzikSelect=NULL;}//mod
  if (fLayoutSelect) {delete fLayoutSelect; fLayoutSelect=NULL;}
  if (fHorSelect) {delete fHorSelect; fHorSelect=NULL;}

  if (fGuzikQuit) {delete fGuzikQuit; fGuzikQuit=NULL;}
  if (fWpisLiczbe) {delete fWpisLiczbe; fWpisLiczbe=NULL;}
  if (fGuzikGiveNumber) {delete fGuzikGiveNumber; fGuzikGiveNumber=NULL;}
  if (fLayoutNumer) {delete fLayoutNumer; fLayoutNumer=NULL;}
  if (fHorNumb) {delete fHorNumb; fHorNumb=NULL;}
  if (fGuzikPrevious) {delete fGuzikPrevious; fGuzikPrevious=NULL;}
  if (fGuzikNext) {delete fGuzikNext; fGuzikNext=NULL;}
  if (fGuzikThisOne) {delete fGuzikThisOne; fGuzikThisOne=NULL;}
  if (fGuzikPreviousSelected) {
     delete fGuzikPreviousSelected; fGuzikPreviousSelected=NULL;
    }
  if (fGuzikNextSelected) {delete fGuzikNextSelected; fGuzikNextSelected=NULL;}

  if (fLayout) {delete fLayout; fLayout=NULL;}
  if (fVert) {delete fVert; fVert=NULL;}

 }
/* ^^^^^^^^^^^^^^^^^ */

/*******************************
  Wodotrysk 
-----------------------------*/
  Bool_t TGOdpowiedz04::ProcessMessage(Long_t msg, Long_t parm1, Long_t )
 {
 
// Process events generated by the buttons in the frame.
   switch (GET_MSG(msg))
    {
     case kC_COMMAND:
       switch (GET_SUBMSG(msg))
         {
          case kCM_BUTTON:
             kNacisniety = (Int_t)parm1;
             if (parm1 == 6) { // numer przypadku
                 strcpy(cKtoryNastepny, fTekst->GetString());
                }
               else if (parm1 == 8) { // select : trigger chanel and pulse no

                  kRadioButtomChannel = -1;
                  for (Int_t i=0; i<8; i++) {
                    if (fRadioCh[i]->IsDown()) { kRadioButtomChannel = i; break;}
                   }

                  kRadioButtomPulseNo = -1;
                  for (Int_t i=0; i<5; i++) {
                    if (fRadioPulseNo[i]->IsDown()) { kRadioButtomPulseNo = i; break;}
                   }
                  printf("TGOdpowiedz04::ProcessMessage: kRadioButtomChannel=%4d  kRadioButtomPulseNo=%4d\n",
                     kRadioButtomChannel, kRadioButtomPulseNo);
                }
             break;
          default:
          break;
         }
     default:
     break;
    }

  theAppl->Terminate(0);

  return kTRUE;
 }
/* ^^^^^^^^^^^^^^^^^ */



