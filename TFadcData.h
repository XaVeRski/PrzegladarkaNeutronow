/*
  header TFadcData.h for
  class/structure TFadcData.cpp to keep one FADC for one EAS event data

  J.Szabelski 15/1/2003, 14/12/2003
  1/1/2016
*/

#ifndef JS_FADC_DATA_02
#define JS_FADC_DATA_02

#define N_FADC_0 32768

/* 0000000000000000000000000000000000000000000 */
  class TFadcData
    : public TObject
 {
  public:
    UChar_t FADC[N_FADC_0];   //FADC data

        TFadcData(void);
       ~TFadcData(void);

// void CopyTo(TFadcData *G);
   void CopyTo(TFadcData *G, Int_t i0 = 0, Int_t i1 = N_FADC_0);
   void FromArray2FADC(UChar_t *a);
   void Reset(void);

/* =========================================== */
   public:
   ClassDef(TFadcData,0);
 };

#endif


