// program rysujacy przebiegi FADC
// wymaga dolaczenia class:
//  TFadcData.cpp TRysFADC.cpp
//  TGOdpowiedz04.cpp TGGuzikiJS.cpp
//
// J.Szabelski 
// w poprzednich wersjach: 31/5/2005, 20/6/2005, 25/11/2012, 28/12/2012
// w wersji przebiegF.c: 20/1/2013
// K. Wojczuk w wersji przebiegKW02.c: 22/10/2015, 21/3/2016
// J.Szabelski w wersji przebiegKW04.c:  1/1/2016, 23/3/2016

   TReference *ref;
#include "startJS04.C"


   TFile *Tfile;
   TGGuzikiJS *G;
   TSelected *Sel;
   TWybor *Wybor;

   Int_t NoOfN;

   TRysFADC *Plot;
// TFadcTests *Test;   // klasa do testowania przybiegow, klasyfikacji pulsow

   TPad *Pads[8];

   FILE *przegladane;
   FILE *wybrane; //mod

// TEventPiki *evPiki;
// TCzytPikiTree *pikiROOT;

   void przebiegKW04(void);
   Int_t WyborGuzikiem(const Int_t iAktualny);
   void dane_wejsciowe(void);

// ****************************
  void przebiegKW04(void)
 {
    // tablice do nazw procedury wybierajacej Pad do powiekszenia

  ref = new TReference();

  cWybrany = NULL;

//evPiki = new TEventPiki();

  dane_wejsciowe(); // znajduje plik i TTree oraz numery 'selected events'

  printf("przebiegKW04: po dane_wejsciowe\n");
  if (!Sel->GetStatus()<0) {return;};
  printf("przebiegKW04: przed proceduryStartowe()\n");
  proceduryStartowe(); // startuje TADCn, liste, galezie
  printf("przebiegKW04: po proceduryStartowe()\n");

  Int_t NSelected = Sel->GetNSel();
  
//Wybor = new TWybor(Sel, TAp, wybrane, NoOfN);
  Wybor = new TWybor(ref);
   
//evntRys = new TEventRys(ListFADC, pikiROOT, Sel);
  evntRys = new TEventRys(ref);


  Plot = NULL;

  Int_t i = 0;


//for (Int_t i=0;i<NoOfN;i++) {
  printf("**************** i=%d\n",i);

  Int_t kSkok;


  petlaW:
  printf("i=%d\n",i);

  fprintf(przegladane, "%6d \n", i);

//gObjectTable->Print();

  ref->Reset();   // reset of description of event (part of ref content)

         // rysowanie przypadkow:
  Tfile->cd();
  br_ch_czas->GetEntry(i);
  br_t70->GetEntry(i);
  for (Int_t ii = 0; ii<8; ii++) {
    br_allFADC[ii]->GetEntry(i);
   }
  ref->nrPrzypadku = i;  // nr wczytanego przypadku

//Plot = evntRys->makePlots(i, Evnt);  // to draw plots
  Plot = evntRys->makePlots(i);  // to draw plots


      // selekcja nastepnego przypadku
//kSkok =  Wybor->WyborGuzikiem(i, Evnt);
  kSkok =  Wybor->WyborGuzikiem(i);
  i = Wybor->kNowy;
  if (kSkok == 2) {goto petlaW;}
      

  printf("\n number of selected = %d  NoOfEvents=%d \n", Wybor->kSelected, NoOfN);
  printf(" czas zbierania: %ld sek = %g godz \n", fDelta_t70, (Double_t)fDelta_t70/3600.0);

  Zakoncz();
 }


// ****************************************
   void dane_wejsciowe(void)
 {

    // -------------------------
    // pliki z danymi z przebiegow FADC (*.root, wyniki "tlumaczenia")
//Char_t sciezka[128] = {"/fedora_home/js/js/obl/LNGSwPe/"};  // KW
  Char_t sciezka[128] = {"/big_disk/home/js/js/obl/LNGS/LNGSwPe/"}; // JS
  Char_t name[128] = {"gstpbpe001"};

  Char_t filename[128];
  sprintf(filename, "%s%s.root", sciezka, name);

  printf("dane_wejsciowe: name **%s**\n",filename);

  Tfile = new TFile(filename,"readonly");
  if (!Tfile) {
    printf("dane_wejsciowe: Nie ma pliku z danymi pomiarowymi: **%s**\n", filename);
   }
  drzewo = (TTree*)Tfile->Get(name);
  printf("dane_wejsciowe: drzewo=%p\n", (void)drzewo);

  ref->Tfile = Tfile;
  ref->drzewo = drzewo;

    // -------------------------
    // pliki z programu szukajacego automatycznie wielokrotnych neutronow
    // zawierajace m.in. numery wygranych przypadkow
  Char_t sciezkaSelected[128] = {"./"};
//Char_t nameSelected[128] = {"neutrony_wPe_00001-21946_111206_0929"};
//Char_t nameSelected[128] = {"wybrane_"};
  Char_t nameSelected[128] = {"JS_00000_00150_wybrane"};
  Char_t filenameSelected[128];
//sprintf(filenameSelected, "%s%s%s.txt", sciezkaSelected, nameSelected, name);
  sprintf(filenameSelected, "%s%s.txt", sciezkaSelected, nameSelected);
//  sprintf(filenameSelected, "%s%s.txt",  nameSelected, name);

  printf("dane_wejsciowe: Selected (ASCII): **%s**\n", filenameSelected);

  Sel = new TSelected(filenameSelected);
  ref->Sel = Sel;

  if (Sel->GetStatus() != 1 ) {
    printf("dane_wejsciowe: nie ma pliku: **%s**  ERROR\n", filenameSelected);
   }
  printf("dane_wejsciowe: Sel=%p\n", Sel);
  ref->Sel = Sel;


  Long_t t70_now;
  t70_now = time(0);
  Tt70_2dattime *td = new Tt70_2dattime();
  Char_t time_now_UTC[16];
  td -> t70_2UTCDateTime(t70_now, time_now_UTC);
  time_now_UTC[14] = (Char_t)0;
  printf("dane_wejsciowe: t70_now = %ld  czas=%s UTC\n", t70_now, time_now_UTC);
  delete td; td = NULL;

    // -------------------------
  Char_t cPrzegladane[128];
  sprintf(cPrzegladane,"przegladane_%s_%s.txt", name, time_now_UTC);
  przegladane = fopen( cPrzegladane ,"w");
  printf("dane_wejsciowe: przegladane=%p\n", przegladane);
  fprintf(przegladane, "#przegladane: dane wejsciowe: %s    czas UTC: %s     t70 = %ld s\n", name, time_now_UTC, t70_now);
  ref->przegladane = przegladane;

    // -------------------------
  Char_t cWybrane[128]; //mod
  sprintf(cWybrane,"wybrane_%s_%s.txt", name, time_now_UTC);
  wybrane = fopen( cWybrane ,"w");
  printf("dane_wejsciowe: wybrane=%p\n", wybrane);
  fprintf(wybrane, "#wybrane: dane wejsciowe: %s    czas UTC: %s     t70 = %ld s\n", name, time_now_UTC, t70_now);
  ref->wybrane = wybrane;


//pikiROOT = new TCzytPikiTree(sciezkaSelected, name);

 }




