/*
  header TPrzemial.h for
  class/structure TPrzemial.cpp to analyse FADC data

  J.Szabelski 5/4/2008, 2/6/2008
*/

#ifndef JS_PRZEMIAL_
#define JS_PRZEMIAL_

#include "TReference.h"

/* 0000000000000000000000000000000000000000000 */
  class TPrzemial
    : public TObject
 {
  public:
   TReference *ref;

    TPrzemial(TReference *qref);
   ~TPrzemial(void);

   void UsunPiki(void);
   Int_t UsunPikiPrzebiegu(TFadcData *fadc);
   void Poziom0_50_100(void);
   Float_t ZnajdzPoziom0(TFadcData *fadc, Int_t i1, Int_t i2);
   void ZnajdzTrigger(void);
   Bool_t CzyTrigger(TFadcData *fadc, Int_t iT, Float_t level);
   void TriggerOK(void);
   Bool_t CzyTriggerOK(TFadcData *fadc, Int_t iT);
   void MaxADCzPolozeniem(void);    // w calym obszarze
   void Max5wPiku(void);   // max usrednione w obszarze piku
   void DlugoscImpulsu(void);
   Int_t LiczDlugoscImpulsu(TFadcData *fadc, UChar_t kPoziom);
   Int_t SzukajInnychImpulsow(void);  // zwraca liczbe impulsow razem z triggerowym w Evencie
   Int_t LiczbaImpulsow(TFadcData *fadc, Float_t level);

/* =========================================== */
   public:
   ClassDef(TPrzemial,0);
 };

#endif


