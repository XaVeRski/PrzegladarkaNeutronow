/*
   TJSsubT70.cpp   (based on TJSsub.cpp)
 procedures (several C++/ROOT classes) for many purposes:
    - conversion character string time <--> t70
    - conversion of integers YY, MM, DD, hh, mm, ss <--> t70

 t70 is always in UTC (as a number of seconds from UTC 0:0:0 at 0 Jan 1970)
 time can be UTC or local

     Jacek Szabelski
     28/9/2003, modyf. 1/11/2007, 10/5/2010
*/

/* classes:
   class Tch2tm // ASCII to tm structure
   class Tch2t70UTC     // ASCII time (UTC) to UTC t70
   class Tch2t70Loc      // ASCII time (local) to UTC t70
   class Tch2t70        // ASCII time (local or UTC)  to UTC t70

   class Tt70_2dattime  // t70  to (UCT or local) ASCII time
   class Tdate2t70     // integers: YYYY, MM, DD, hh, mm, ss (UTC or local)
                 // to t70 (UTC)
   class Tt70_2date   // t70 to (UTC or local) time in integer form:
                   //  YYYY, MM, DD, hh, mm, ss

   class Tascii_date // converts ASCII time to integer time and opposite:
                 // yyyymmddhhmmss <--> (integers: YYYY, MM, DD, hh, mm, ss)
*/

#include "TJSsubT70.h"

   ClassImp(Tch2tm);
   ClassImp(Tch2t70UTC);
   ClassImp(Tch2t70Loc);
   ClassImp(Tch2t70);
   ClassImp(Tt70_2dattime);
   ClassImp(Tdate2t70);
   ClassImp(Tt70_2date);
   ClassImp(Tascii_date);


// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ time t70 manipulations
// *****************************
// converts ascii YYYYMMDDhhmmss to tm structure
// -------------------------------------
    void Tch2tm::Ch2tm(const Char_t *yyyymmddhhmmss, tm &jstm)
 {
  jstm.tm_year = Ch2Int(&yyyymmddhhmmss[0],4)-1900;
  jstm.tm_mon  = Ch2Int(&yyyymmddhhmmss[4],2)-1;
  jstm.tm_mday = Ch2Int(&yyyymmddhhmmss[6],2);
  jstm.tm_hour = Ch2Int(&yyyymmddhhmmss[8],2);
  jstm.tm_min  = Ch2Int(&yyyymmddhhmmss[10],2);
  jstm.tm_sec  = Ch2Int(&yyyymmddhhmmss[12],2);
//printf("in TJSsubT70 Tch2tm::Ch2tm: jstm.tm_year = %d\n",jstm.tm_year);
 }
 // ...............................
    Int_t Tch2tm::Ch2Int(const Char_t *ch, const Int_t n)
 {
  Char_t chch[n+1];
  Int_t k;
  strncpy(chch,ch,n);
  chch[n] = 0;
  sscanf(chch,"%d",&k);
//printf("in TJSsubT70 Tch2tm::Ch2Int: %d\n",k);
  return k; 
 }
// *****************************
// converts ascii YYYYMMDDhhmmss to t70 (UTC)  (assuming UTC time)
// -------------------------------------
    Tch2t70UTC::Tch2t70UTC(void) {C = new Tch2tm();}
    Tch2t70UTC::~Tch2t70UTC(void) {delete C; C=NULL;}
    Long_t Tch2t70UTC::UTCDateTime2t70(const Char_t *yyyymmddhhmmss)
 {
//printf("in TJSsubT70 Tch2t70UTC::UTCDateTime2t70:>>%s<<\n",yyyymmddhhmmss);
  C->Ch2tm(yyyymmddhhmmss, jstm);
//printf("in TJSsubT70 Tch2t70UTC::UTCDateTime2t70: jstm.tm_year=%d\n",
//   jstm.tm_year);
  return (Long_t)timegm(&jstm);
 }
// ^^^^^^^^^^^^^^^^^^

// *****************************
// converts ascii YYYYMMDDhhmmss to t70 (UTC)  (assuming Local time)
// -------------------------------------
    Tch2t70Loc::Tch2t70Loc(void) {C = new Tch2tm();}
    Tch2t70Loc::~Tch2t70Loc(void) {delete C; C=NULL;}
    Long_t Tch2t70Loc::LocDateTime2t70(const Char_t *yyyymmddhhmmss)
 {
//printf("in TJSsubT70 Tch2t70Loc::LocDateTime2t70:>>%s<<\n",yyyymmddhhmmss);
  C->Ch2tm(yyyymmddhhmmss, jstm);
//printf("in TJSsubT70 Tch2t70Loc::LocDateTime2t70: jstm.tm_year=%d\n",
//   jstm.tm_year);
//return (Long_t)mktime(&jstm);
  return (Long_t)timelocal(&jstm);   // the same as above line
 }
// ^^^^^^^^^^^^^^^^^^

// *****************************
// converts ascii YYYYMMDDhhmmss to t70 (UTC)  (assuming Local time)
// -------------------------------------
    Tch2t70::Tch2t70(void) {C = new Tch2tm();}
    Tch2t70::~Tch2t70(void) {delete C; C=NULL;}
    Long_t Tch2t70::LocDateTime2t70(const Char_t *yyyymmddhhmmss)
 {
//printf("in TJSsubT70 Tch2t70::LocDateTime2t70:>>%s<<\n",yyyymmddhhmmss);
  C->Ch2tm(yyyymmddhhmmss, jstm);
//printf("in TJSsubT70 Tch2t70::LocDateTime2t70: jstm.tm_year=%d\n",
//   jstm.tm_year);
//return (Long_t)mktime(&jstm);
  return (Long_t)timelocal(&jstm);   // the same as above line
 }
    // ......................
    Long_t Tch2t70::UTCDateTime2t70(const Char_t *yyyymmddhhmmss)
 {
//printf("in TJSsubT70 Tch2t70::UTCDateTime2t70:>>%s<<\n",yyyymmddhhmmss);
  C->Ch2tm(yyyymmddhhmmss, jstm);
//printf("in TJSsubT70 Tch2t70::UTCDateTime2t70: jstm.tm_year=%d\n",
//   jstm.tm_year);
  return (Long_t)timegm(&jstm);
 }

// ^^^^^^^^^^^^^^^^^^

  
// *****************************
// converts t70 (UTC) to ascii YYYYMMDDhhmmss (to UTC or local time)
// -------------------------------------
    void Tt70_2dattime::t70_2UTCDateTime(Long_t t70, Char_t *yyyymmddhhmmss)
 {
  jstm = *gmtime(&t70);

  sprintf(yyyymmddhhmmss,
     "%04d%02d%02d%02d%02d%02d",
     jstm.tm_year + 1900,jstm.tm_mon + 1,
     jstm.tm_mday,jstm.tm_hour,jstm.tm_min,jstm.tm_sec);
//             printf("t70_2date:##%s##\n",yyyymmddhhmmss);
 }
    // ...............................
    void Tt70_2dattime::t70_2LocDateTime(Long_t t70, Char_t *yyyymmddhhmmss)
 {
  jstm = *localtime(&t70);

  sprintf(yyyymmddhhmmss,
     "%04d%02d%02d%02d%02d%02d",
     jstm.tm_year + 1900,jstm.tm_mon + 1,
     jstm.tm_mday,jstm.tm_hour,jstm.tm_min,jstm.tm_sec);
//             printf("t70_2date:##%s##\n",yyyymmddhhmmss);
 }

// ^^^^^^^^^^^^^^^^^^


// *****************************
// converts int Yyyy,Month,Day,hh,mm,ss to t70 (assuming UTC time)
// -------------------------------------
  // ..................................
    Long_t Tdate2t70::UTCDate2t70(const Int_t Yyyy, const Int_t Month,
     const Int_t Day, const Int_t hh, const Int_t mm, const Int_t ss)
 {
  ii2tm(Yyyy,Month,Day, hh,mm,ss);
  return (Long_t)timegm(&jstm);
 }
  // ..................................
    Long_t Tdate2t70::LocDate2t70(const Int_t Yyyy, const Int_t Month,
     const Int_t Day, const Int_t hh, const Int_t mm, const Int_t ss)
 {
  ii2tm(Yyyy,Month,Day, hh,mm,ss);
//return (Long_t)mktime(&jstm);
  return (Long_t)timelocal(&jstm);   // the same as above line
 }
  // ..................................

    void Tdate2t70::ii2tm(const Int_t Yyyy, const Int_t Month,
     const Int_t Day, const Int_t hh, const Int_t mm, const Int_t ss)
 {
  jstm.tm_year = (Int_t)(Yyyy-1900);
  jstm.tm_mon  = Month-1;
  jstm.tm_mday = Day;
  jstm.tm_hour = hh;
  jstm.tm_min  = mm;
  jstm.tm_sec  = ss;
//printf("in TJSsubT70 Tdate2t70::ii2tm: (*jstm).tm_year = %d\n",(*jstm).tm_year);
 }

// ^^^^^^^^^^^^^^^^^^

// *****************************
// converts t70 (UTC)
//  to int Yyyy,Month,Day,hh,mm,ss (for local or  UTC time)
// to be called like:
//    Int_t Yyyy,Month,Day,hh,mm,ss;
//   .... ->t70_2dateUTC(t70,&Yyyy,&Month,&Day,&hh,&mm,&ss);
// -------------------------------------
  // ..................................
    void Tt70_2date::t70_2dateUTC(const Long_t t70,
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss)
 {
  jstm = *gmtime(&t70);
  jstm2int(Yyyy,Month,Day,hh,mm,ss);
//printf("TJSsubT70 in Tt70_2date::UTCt70_2date:");
//printf("Yyyy,Month,Day,hh,mm,ss = %4d-%02d-%02d %02d:%02d:%02d \n",
//     *Yyyy,*Month,*Day,*hh,*mm,*ss);
 }
  // ..................................
    void Tt70_2date::t70_2dateLoc(const Long_t t70,
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss)
 {
  jstm = *localtime(&t70);
  jstm2int(Yyyy,Month,Day,hh,mm,ss);
 }
  // ..................................
   void Tt70_2date::jstm2int(
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss)
 {   
  Yyyy  = jstm.tm_year+1900;
  Month = jstm.tm_mon+1;
  Day   = jstm.tm_mday; 
  hh    = jstm.tm_hour;
  mm    = jstm.tm_min;
  ss    = jstm.tm_sec;
 }
// ^^^^^^^^^^^^^^^^^^

// *****************************
// converts character (ASCII) time yyyymmddhhmmss
//     to int Yyyy,Month,Day,hh,mm,ss (for local or  UTC time)
//  and
//   opposite
// to be called like:
//    Int_t Yyyy,Month,Day,hh,mm,ss;
//    Char_t yyyymmddhhmmss[16]; 
//   .... ->ascii2date(yyyymmddhhmmss,&Yyyy,&Month,&Day,&hh,&mm,&ss);
//   .... ->date2ascii(Yyyy,Month,Day,hh,mm,ss,yyyymmddhhmmss);
// -------------------------------------
  // ..................................
    void Tascii_date::ascii2date(const Char_t *yyyymmddhhmmss,
       Int_t &Yyyy, Int_t &Month, Int_t &Day,
       Int_t &hh, Int_t &mm, Int_t &ss)
 {
  Yyyy  = Ch2Int(&yyyymmddhhmmss[0],4);
  Month = Ch2Int(&yyyymmddhhmmss[4],2);
  Day   = Ch2Int(&yyyymmddhhmmss[6],2);
  hh    = Ch2Int(&yyyymmddhhmmss[8],2);
  mm    = Ch2Int(&yyyymmddhhmmss[10],2);
  ss    = Ch2Int(&yyyymmddhhmmss[12],2);
 } 
  // ..................................
    void Tascii_date::date2ascii(const Int_t Yyyy, const Int_t Month,
       const Int_t Day, const Int_t hh, const Int_t mm, const Int_t ss,
       Char_t *yyyymmddhhmmss)
 {
  sprintf(yyyymmddhhmmss,
    "%04d%02d%02d%02d%02d%02d",
    Yyyy,Month,Day,hh,mm,ss); 
  yyyymmddhhmmss[14] = (Char_t)0;
 }
 // ...............................
    Int_t Tascii_date::Ch2Int(const Char_t *ch, const Int_t n)
 {
  Char_t chch[n+1];
  Int_t k;
  strncpy(chch,ch,n);
  chch[n] = 0;
  sscanf(chch,"%d",&k);
//printf("in TJSsubT70 Tch2tm::Ch2Int: %d\n",k);
  return k;
 }
// ^^^^^^^^^^^^^^^^^^
  

  

