/*
  header TReference.h for
  class/structure TReference.cpp zawierajacy informacje
   dotyczaca opracowania wynikow 
   w tym polozenia poczatkow pikow

  J.Szabelski 17/01/2016
*/

#ifndef JS_REF_N_01
#define JS_REF_N_01

#define N_FADC_K 8
#define N_IMP_L 4096

#include <stdio.h>

#include "TList.h"
#include "TFile.h"
#include "TTree.h"
#include "TApplication.h"


#include "TEvent.h"
#include "TSelected.h"
//#include "TEventRys.h"
//#include "TPrzemial.h"


/* 0000000000000000000000000000000000000000000 */
  class TReference
 {
  private:

  public:

   FILE *przegladane;
   FILE *wybrane;

   TList *ListFADC;

   TApplication *TAp;
   TFile *Tfile;
   TTree *drzewo;
   Int_t NoOfN;

   TEvent *Evnt;
   TSelected *Sel;
// TEventRys *evntRys;
// TPrzemial *rob;

   Int_t nrPrzypadku; // nr przeczytanego przypadku do klasy TEvent

   Long_t t70; // czas t70 z PC
   Int_t kNrPrzypadku;  // kolejny nr przypadku w serii
   Float_t Poziom0_50_100[N_FADC_K];
   Bool_t bPoziom0;   // if TRUE, Poziom0_50_100[8] were evaluated
   Bool_t bJestTrigger; 
   Bool_t bTrigger[N_FADC_K];
   Bool_t bTriggerOK[N_FADC_K];  // dlugi czas narastania
   Int_t fadcLiczbaImp[N_FADC_K];  // liczba impulsow w przebiegach
   Int_t fadcLiczbaNarastanImp[N_FADC_K]; // liczba narastan impulsow liczona, gdy jest Imp, 
                                  // tj. gdy fadcLiczbaImp[k] > 0
                                 // eliminowane sa wolne narastania
                                 // dodane sa wielokrotne narastania
   UShort_t poczImp[N_FADC_K][N_IMP_L]; // polozenia startu impulsow
   UChar_t ucMaxADC[N_FADC_K];  // wartosc  max (w calym przebiegu)
   Int_t ixMaxADC[N_FADC_K];   // polozenie max (w calym przebiegu)
   Float_t srMaxPik[N_FADC_K];  // srednia wartosc kilku najwiekszych kolejnych ADC (patrz: TPrzemial::Max5wPiku)
   Int_t ixSrMaxPik[N_FADC_K];  // polozenie srodka przedzialu sredniej wartosci powyzej
   Bool_t bJestMaxPik;

   Int_t kDlugoscImpPik[N_FADC_K]; // TPrzemial::DlugoscImpulsu 
   Bool_t bSaDlugosciPik;

        TReference(void);
       ~TReference(void){};

   void Reset(void);

/* =========================================== */
   public:
   ClassDef(TReference,0);
 };

#endif


