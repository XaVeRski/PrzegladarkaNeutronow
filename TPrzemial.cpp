/*
  class/structure TPrzemial.cpp to analyse FADC data

  J.Szabelski 5/4/2008
*/

#include"TPrzemial.h"

   ClassImp(TPrzemial);

// **********************
  TPrzemial::TPrzemial(TReference *qref)
 {
  ref = qref;
 }

  TPrzemial::~TPrzemial(void){}
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TPrzemial::UsunPiki(void)
 {
//Int_t i = 6; {
  for (Int_t i = 0;i<8;i++) {
    Int_t kZmian = UsunPikiPrzebiegu(ref->Evnt->przeb2[i]);
    if (kZmian>0) {
      printf("TPrzemial::UsunPiki: tor=%1d kZmian=%d\n",i,kZmian);
     }
   }
 }
// ^^^^^^^^^^^^^^^^^^^^^
// **********************
// pojedyncze piki zamienia na srednia wartosc z sasiednich binow
//   jesli z kazdej strony jest roznica > 20
// zwraca liczbe wprowadzonych zmian
// ---------------------
  Int_t TPrzemial::UsunPikiPrzebiegu(TFadcData *fadc)
 {
  const Int_t Rr = 20;
  UChar_t *uc; uc = fadc->FADC;
  Int_t kZmian = 0;
  Int_t nn = N_FADC_0 - 2;
  Int_t a0, a1, a2;
  for (Int_t i=0; i<nn; i++) {
    a0 = (Int_t)(*uc);
    a1 = (Int_t)(*(uc+1));
    a2 = (Int_t)(*(uc+2));
    if (TMath::Abs(a0-a1) > Rr) {
      if (TMath::Abs(a1-a2) > Rr) {
        if (TMath::Abs(a0 - a2) < Rr) {
          a1 = (Int_t)((Float_t)(a0 + a2)/2.+0.5);
          *(uc+1) = (UChar_t)a1;
          kZmian++;
         }
       }
     } 
   }
  return kZmian;
 }
// ^^^^^^^^^^^^^^^^^^^^^
// **********************
// oblicza poziom zerowy FADC jako srednia
//  w zadanym przediale rejestracji
//  srednie sa zaznaczone w TOneEvent
// ---------------------
   void TPrzemial::Poziom0_50_100(void)
 {
  if (ref->bPoziom0) {return;}
  for (Int_t i = 0;i<8;i++) {
    ref->Poziom0_50_100[i] = ZnajdzPoziom0(ref->Evnt->przeb2[i],50,100);
   }
  ref->bPoziom0 = kTRUE;
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
   Float_t TPrzemial::ZnajdzPoziom0(TFadcData *fadc, Int_t i1, Int_t i2)
 {
  Int_t k0 = 0;
  Int_t k = 0;
  UChar_t *uc; uc = fadc->FADC + i1;
  for (Int_t i=i1; i<=i2; i++) {
    k0 += (Int_t)(*uc++);
    k++;
   }
  Float_t p0 = (Float_t)k0 / (Float_t)(k);
  return p0;
 }
// ^^^^^^^^^^^^^^^^^^^^^


// **********************
// szuka toru (torow) dajacych trigger
//  tory sa zaznaczone w TOneEvent
// ---------------------
  void TPrzemial::ZnajdzTrigger(void)
 {
  if (!ref->bPoziom0) {Poziom0_50_100();}
  const Int_t ixTrig = 1536;
  const Int_t triggerLevel = 30;
  for (Int_t i = 0;i<8;i++) {
    Float_t levelTrig = ref->Poziom0_50_100[i] + (Float_t)triggerLevel;
    ref->bTrigger[i] = CzyTrigger(ref->Evnt->przeb2[i], ixTrig, levelTrig);
   }
  ref->bJestTrigger = kTRUE;
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  Bool_t TPrzemial::CzyTrigger(TFadcData *fadc, Int_t iT, Float_t level)
 {
  UChar_t cLevel = (UChar_t)level;
  UChar_t *uc; uc = fadc->FADC + iT;
  Bool_t btrigger = kFALSE;
  if (*uc > cLevel) {btrigger = kTRUE;}
  if (*(uc+1) > cLevel) {btrigger = kTRUE;}
  return btrigger;
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
//  tylko dla triggerow
//   czas szybkiego narastania powinien byc ok. 2 mikrosek (20 pikseli)
// ---------------------
  void TPrzemial::TriggerOK(void)
 {
  Bool_t *bb; bb = ref->bTriggerOK;
  for (Int_t i=0;i<8;i++) {*bb++ = kFALSE;}
  if (!ref->bJestTrigger) {ZnajdzTrigger();}

  const Int_t ixTrig = 1536;
  bb = ref->bTrigger;
  for (Int_t i = 0;i<8;i++) {
    if (*bb) {
      ref->bTriggerOK[i] = CzyTriggerOK(ref->Evnt->przeb2[i], ixTrig);
     }
    bb++;
   }
 }
// ^^^^^^^^^^^^^^^^^^^^^
// **********************
  Bool_t TPrzemial::CzyTriggerOK(TFadcData *fadc, Int_t iT)
 {
  UChar_t *uc; uc = fadc->FADC + iT + 2 ;
  Bool_t btrigger = kTRUE;
  for (Int_t i=0; i<12; i+= 2) {
    if ( (*(uc+2)) <= (*uc) ) {
      btrigger = kFALSE;
      break;
     }
    uc++;
   }
  return btrigger;
 }
// ^^^^^^^^^^^^^^^^^^^^^



// **********************
// szuka max ADC i pierwszego polozenia MaxADC w calym obszarze
//  wyniki w TReference
// ---------------------
  void TPrzemial::MaxADCzPolozeniem(void)
 {
  for (Int_t i = 0;i<8;i++) {
    UChar_t *uc; uc = ref->Evnt->przeb2[i]->FADC;
    UChar_t ucmax = 0;
    Int_t polozenie = 0;
    for (Int_t j=0; j< N_FADC_0 ; j++) {
      if (*uc > ucmax) { ucmax = *uc; polozenie = j;}
      uc++;
     }
    ref->ucMaxADC[i] = ucmax;
    ref->ixMaxADC[i] = polozenie;
   }
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
// szuka max ADC i pierwszego polozenia MaxADC w okolicy triggera
//  jako srednia wartosc z nSasiednich wartosci.
//  polozenie dotyczy srodka przedzialu.
//  wyniki w TOneEvent
// ---------------------
  void TPrzemial::Max5wPiku(void)
 {
  const Int_t ixTrig = 1536;
  const Int_t ixPikEnd = ixTrig + 2000;
  const Int_t nSasiednich = 7;
  const Int_t nSrodek     = 3;
  for (Int_t i = 0;i<8;i++) {
    Int_t SumaMax = 0;
    Int_t Suma = 0;
    Int_t polozenie = 0;
    UChar_t *uc; uc = ref->Evnt->przeb2[i]->FADC + ixTrig;
         // obliczenia poczatkowe
    for (Int_t j=0; j<nSasiednich; j++) {
      Suma += (Int_t)(*(uc+j));
     }
    SumaMax = Suma;
    polozenie = ixTrig;
         // petla wlasciwa:
    for (Int_t j=ixTrig+1; j<=ixPikEnd; j++) {
      Suma += (Int_t)(*(uc+nSasiednich)) - (Int_t)(*uc);
      if (Suma > SumaMax) {
        SumaMax = Suma;
        polozenie = j;
       }
      uc++;
     }

    Float_t fMax = (Float_t)SumaMax / (Float_t)nSasiednich;
    polozenie += nSrodek;
    ref->srMaxPik[i] = fMax;
    ref->ixSrMaxPik[i] = polozenie;
   }
  ref->bJestMaxPik = kTRUE;

 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
//  oblicza dlugosc impulsu na poziomie kPoziomImp
//  dla impulsow zaczynajacych sie w czasie triggera;
//  obliczenia dla wszystkich impulsow pokazujacych impuls
//  w obszarze piku ( piksele 1536 - 2000 ) wiekszy 
//  niz ( (kPoziomImp + 10) < srMaxPik[i] )
// ---------------------
  void TPrzemial::DlugoscImpulsu(void)
 {
  if (!ref->bJestMaxPik) { Max5wPiku();}
  const UChar_t kPoziomImp = 30; 
  const Float_t kMinSrADC = (Float_t)kPoziomImp + 10;

  for (Int_t i = 0;i<8;i++) {
    ref->kDlugoscImpPik[i] = 0;
    if ( ref->srMaxPik[i] > kMinSrADC) {
      ref->kDlugoscImpPik[i] = LiczDlugoscImpulsu(ref->Evnt->przeb2[i], kPoziomImp);
     } 
   }
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  Int_t TPrzemial::LiczDlugoscImpulsu(TFadcData *fadc, UChar_t kPoziom)
 {
  const Int_t ixTrig = 1536;
  const Int_t maxIx = N_FADC_0;

  UChar_t *uc; uc = fadc->FADC;

  Int_t kDl = 0;
  Bool_t bStart = kFALSE;
  Int_t ix = ixTrig - 2;
  uc += ix;
  while ( ix < maxIx) {
    if (bStart) {
       if ( (*uc) < kPoziom) { break;}
       kDl++;
      }
     else {
       if ( (*uc) > kPoziom) {
         kDl++;
         bStart = kTRUE;
        }
      }
    ix++;
    uc++;
   }
  ref->bSaDlugosciPik = kTRUE;

  return kDl;
 }
// ^^^^^^^^^^^^^^^^^^^^^


// **********************
// liczy liczbe impulsow w 8 przebiegach
// ---------------------
  Int_t TPrzemial::SzukajInnychImpulsow(void)
 {
  if (!ref->bPoziom0) {Poziom0_50_100();}
  const Int_t triggerLevel = 30;
  Int_t liczbaImpulsow = 0;
  for (Int_t i = 0;i<8;i++) {
    Float_t levelTrig = ref->Poziom0_50_100[i] + (Float_t)triggerLevel;
    Int_t lImp = LiczbaImpulsow(ref->Evnt->przeb2[i], levelTrig);
//  printf("TPrzemial::SzukajInnychImpulsow:i=%1d lImp=%2d\n",i,lImp);
    ref->fadcLiczbaImp[i] = lImp;
    liczbaImpulsow += lImp;
   }
  return liczbaImpulsow;
 }

// **********************
// liczy liczbe impulsow w przebiegu
// ----------------------
  Int_t TPrzemial::LiczbaImpulsow(TFadcData *fadc, Float_t level)
 {
  UChar_t cLevel = (UChar_t)level;
  UChar_t *uc; uc = fadc->FADC;
  Int_t nImpulsow = 0;      // liczba impulsow
// N_FADC_0
  Int_t nPixeliImp = 50;
  Int_t nPrzerwa = 40;
  // impuls narasta i musi trwac przez min. nPixeliImp z ADC > cLevel
  //  a nastepnie musi byc co najmniej nPrzerwa pixeli z ADC < cLevel
  //          przed nastepnym impulsem
  Bool_t bPowyzej = kFALSE;  // kTRUE, jesli poprzedni impuls mial ADC > cLevel
  Int_t licznikPowyzej = 0;
  Int_t licznikPonizej = 0;
  for (UInt_t i = 0; i< N_FADC_0; i++) {
       //  + + +
    if (bPowyzej) {
       if (*uc > cLevel) { licznikPowyzej++;} // kontunuacja piku
         else {    // poczatek konca impulsu lub jeden z impulsow ADC<cLevel po impulsie
 //        licznikPowyzej = 0;
           licznikPonizej++;
           if (licznikPonizej >= nPrzerwa) {
             bPowyzej = kFALSE;  // to jest koniec impulsu (ADC>cLevel + nPrzerwa)
             if (licznikPowyzej >= nPixeliImp) { // impuls - zliczenie
                nImpulsow++;
//              printf(" ii=%6d nImpulsow=%2d\n",i,nImpulsow);
                licznikPowyzej = 0;
               }
            } 
          }
      }
       //  + + +
     else {
       if (*uc > cLevel) {   // mozliwy poczatek impulsu
          licznikPowyzej++;
          licznikPonizej = 0;
          if (licznikPowyzej >= nPixeliImp) {  // jest impuls
            bPowyzej = kTRUE;
           }
         }
        else {
          licznikPonizej++;
          licznikPowyzej = 0;
         }
      }
       //  + + +
    uc++;
   }
  return nImpulsow;
 }
// ^^^^^^^^^^^^^^^^^^^^^

